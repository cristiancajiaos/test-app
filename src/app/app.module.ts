/* Módulo principal */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/* Componentes de Layout */
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';

/* Componentes de Vistas */
import { HomeComponent } from './components/views/home/home.component';
import { NumbersComponent } from './components/views/numbers/numbers.component';
import { ParagraphsComponent } from './components/views/paragraphs/paragraphs.component';

@NgModule({
  declarations: [
    AppComponent,
    NumbersComponent,
    ParagraphsComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
