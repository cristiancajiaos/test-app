/* Componente de Vista: Paragraphs */

import { Component, OnInit } from '@angular/core';

/* Interfaces */
import { Paragraph } from '../../../interfaces/paragraph';
import { QuantityLetter } from './../../../interfaces/quantityLetters';

/* Servicios */
import { ParagraphsService } from './paragraphs.service';

/* Constantes */
import { LETTERS } from './../../../constants/letters';

@Component({
  selector: 'app-paragraphs',
  templateUrl: './paragraphs.component.html',
  styleUrls: ['./paragraphs.component.scss']
})
export class ParagraphsComponent implements OnInit {
  /* Propiedades */

  /* (1) Arreglo de paragraphs
    Con él se obtene la información del endpoint */
  paragraphs: Paragraph[] = [];

  /* (2) Constante de letras
    Se usa para las columnas de las tablas, y para calcular las letras en los parrafos */
  letters: string[] = LETTERS;

  /* Variables de control de directivas
  Controlan que se muestren o no los datos obtenidos */
  showData: boolean = false;
  loadingData: boolean = false;

  /* Constructor */
  constructor(
    private paragraphsService: ParagraphsService
  ) {

  }

  /* Métodos */
  ngOnInit(): void {

  }

  /* A partir del arreglo de párrafos, se obtiene
    el número de instancias de una letra en un párrafo,
    entregando estos últimos dos como argumento */
  getQuantityLetters(p: Paragraph, str: string): QuantityLetter {
    const quantityLetter: QuantityLetter = {
      letter: str,
      quantity: 0
    };

    const pArr = p.paragraph.toLowerCase().split('');

    pArr.forEach(e => {
      if (e === str) {
        quantityLetter.quantity++;
      }
    });

    return quantityLetter;
  }

  /* A partir de un párrafo como argumento, y usando una expresión regulsr
     se obtiene en él los números absolutos presentes en él. */
  getNumbersParagraph(p: Paragraph): number {
    const regex = new RegExp(/-?\d+/, 'gi');

    if (p.paragraph.match(regex) != null) {
      return p.paragraph.replace('–', '-').match(regex).reduce((previousValue, currentValue) => {
        return previousValue + parseInt(currentValue, 10);
      }, 0);
    } else {
      return 0;
    }
  }

  /* Se obtiene la información del endpoint */
  getParagraphs(): void {
    this.showData = false;
    this.loadingData = true;
    this.paragraphsService.getParagraphs().subscribe(response => {
      this.loadingData = false;
      this.showData = true;
      this.paragraphs = JSON.parse(response.data);
      console.log(this.paragraphs);
    });
  }
}
