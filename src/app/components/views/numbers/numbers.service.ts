/* Servicio Numbers */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

/* Interfaces */
import { Response } from '../../../interfaces/response';

@Injectable({
  providedIn: 'root'
})
export class NumbersService {
  /* Propiedades */

  /* Endpoint para Vista de Números */
  url: string = 'http://patovega.com/prueba_frontend/array.php';

  /* Constructor */
  constructor(
    private http: HttpClient
  ) { }

  /* Métodos */

  /* Servicio para obtener números */
  getNumbers(): Observable<Response> {
    return this.http.get<Response>(this.url)
      .pipe(catchError(this.handleError<Response>('getNumbers', {})));
  }

  /* Método para gestión de errores
     Devuelve un arreglo vacío en caso de error */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
}
