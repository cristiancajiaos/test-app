/* Componente de Vista: Numbers */

import { Component, OnInit } from '@angular/core';

/* Interfaces */
import { QuantityNumbers } from '../../../interfaces/quantityNumbers';

/* Servicios */
import { NumbersService } from './numbers.service';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.scss']
})

export class NumbersComponent implements OnInit {
  /* Propiedades */

  /* (1) Arreglo de numbers:
  Con él se obtiene la información del endpoint */
  numbers: number[] = [];

  /* (2) Arreglo de cantidad de números:
  Con él se llena la tabla de ocurrencias */
  quantityNumbers: QuantityNumbers[] = [];

  /* (3) Arreglo de números ordenados:
  Con él se llena el textbox con los números de menor a mayor */
  orderedNumbers: number[] = [];

  /* Variables de control de directivas
  Controlan que se muestren o no los datos obtenidos */
  showData: boolean = false;
  loadingData: boolean = false;

  /* Constructor */
  constructor(
    private numbersService: NumbersService
  ) {

  }

  /* Métodos */
  ngOnInit(): void {

  }
  /* Ordena un arreglo de enteros de menor a mayor */
  orderMinMax(arr: number[]): number[] {
    const newArr: number[] = [];
    let currentMin: number;
    let indexArr: number;

    function min(arr: number[]): number {
      try {
        if (arr.length === 0) {
          throw('El arreglo no puede estar vacío');
        }
      } catch (error) {
        console.log(`Error: ${error}`);
      }

      let num = arr[0];

      arr.forEach((e) => {
        if (num > e) {
          num = e;
        }
      });

      return num;
    }

    while (arr.length > 0) {
      currentMin = min(arr);
      indexArr = arr.indexOf(currentMin);
      newArr.push(currentMin);
      arr.splice(indexArr, 1);
    }

    return newArr;
  }

  /* A partir de la información obtenida del endpoint,
     se obtiene un arreglo de cantidades para cada número */
  setQuantity(): void {
    this.numbers.forEach((element, index) => {

      const qIndex: number = this.quantityNumbers.findIndex((qEl) => qEl.number === element);

      if (qIndex === -1) {
        this.quantityNumbers.push({
          number: element,
          quantity: 1,
          firstPosition: index,
          lastPosition: index
        });
      } else {
        this.quantityNumbers[qIndex].quantity += 1;
        this.quantityNumbers[qIndex].lastPosition = index;
      }
    });
  }

  /* A partir del arreglo de cantidades para cara número,
     se obtiene un arreglo ordenado de todos los numeros únicos obtenidos en
     la información desde el endpoint */
  setOrderedNumbers(): void {
    this.orderedNumbers = this.orderMinMax(this.quantityNumbers.map(e => e.number));
  }

  /* Se obtiene la información del endpoint,
     con la cual se obtienen los dos otros arreglos que se van a usar  */
  getNumbers() {
    this.quantityNumbers = [];
    this.showData = false;
    this.loadingData = true;
    this.numbersService.getNumbers()
      .subscribe(response => {
        this.loadingData = false;
        this.showData = true;
        this.numbers = response.data;
        this.setQuantity();
        this.setOrderedNumbers();
      });
  }
}
