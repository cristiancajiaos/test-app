/* Componente de Layout: Header */
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  /* Propiedades */

  /* (1) Ruta
  Almacena la ruta actual. */
  route: string;

  /* Constructor */

  constructor(public location: Location, public router: Router) {
    /* Al inicio, se va a ejecutar el evento que obtiene la ruta actual.
       Esto va a ser relevante para establecer en la barra de navegación, mediante clase active, la ruta actual */
    router.events.subscribe(val => {
      if (location.path() !== '') {
        this.route = location.path();
      } else {
        this.route = 'home';
      }
    });
  }

  /* Métodos */

  ngOnInit(): void {}

}
