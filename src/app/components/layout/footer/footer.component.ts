/* Componente de Layout: Footer */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  /* Propiedades */

  /* (1) Fecha
     Su único propósito es establecer el año actual en el footer */
  today = new Date();

  /* Constructor */
  constructor() { }

  /* Métodos */
  ngOnInit(): void {}
}
