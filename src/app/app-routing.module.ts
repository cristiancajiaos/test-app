/* Módulo de ruteo */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Componentes de Vistas */
import { HomeComponent } from './components/views/home/home.component';
import { NumbersComponent } from './components/views/numbers/numbers.component';
import { ParagraphsComponent } from './components/views/paragraphs/paragraphs.component';

/* Las rutas principales son tres:
   - Home: Ruta inicial. Cuando se visita la ruta vacía, redirige a ella
   - Numbers: Ruta a la vista de Números
   - Paragraphs: ruta a la vista de Párrafos
*/
const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'numbers', component: NumbersComponent},
  {path: 'paragraphs', component: ParagraphsComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
