/* Interface QuantityNumbers
   Para las filas de lta bla de la vista Numbers */

export interface QuantityNumbers {
  number: number;
  quantity: number;
  firstPosition: number;
  lastPosition: number;
}
