/* Interface QuantityLetter
   Para los registros de {letras,cantidad} en la tabla de la vista Paragraphs */

export class QuantityLetter {
  letter: string;
  quantity: number;
}
