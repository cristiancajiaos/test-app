/* Interface Paragraph */

export interface Paragraph {
  paragraph: string;
  number: number;
  hasCopyright: boolean;
}
