/* Interface Response
   Para los resultados desde los endpoints */

export interface Response {
  data?: any;
  error?: any;
  response?: any;
}
