# Prueba de postulación para Front-end

Este proyecto para la prueba de postulación fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1, pero adaptado para Angular 8 (8.2.3). 

## Instrucciones de instalación 

1. Clonar el repositorio (`git clone https://bitbucket.org/cristiancajiaos/test-app.git`)
2. Instalar las dependencias (`npm install`)
3. Una vez instaladas las dependencias, el proyecto va a estar listo para trabajar con él. 

## Comandos

- `ng serve` o `ng serve -o` para ejecutar la aplicación en entorno local. Correrá en `http://localhost:4200/`

## Contacto

- cristian.cajiao@alumnos.usm.cl
